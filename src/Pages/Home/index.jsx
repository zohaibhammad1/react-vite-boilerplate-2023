import {
  Container,
  Row,
  Col,
  ProgressBar,
  Dropdown,
  Button,
} from "react-bootstrap";
import ScoreBadge from "Assets/Images/new-assets/silver-s.png";
import "./style.scss";
import userIcon from "Assets/Images/profile-icon.svg";
import RankBadge from "Assets/Images/Home/rank-badge.png";
import {
  BadgesModal,
  Footer,
} from "Components";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { logout } from "Utils";

const Home = () => {
  const [modalShow, setModalShow] = useState(false);

  const navigate = useNavigate();
  const { t } = useTranslation();

  return (
    <>
      <div className="homepage">
        <Container fluid className="p-0">
          <Row className="home-pt">
            <Col sm="6" className="p-0">
              <div className="score-wrapper">
                <div className="badge-header d-flex align-items-center">
                  <img
                    className="score-badge"
                    onClick={() => setModalShow(true)}
                    src={ScoreBadge}
                    alt=""
                  />
                  <div className="custom-progress">
                    <ProgressBar now={50} />
                  </div>
                  <div className="score">
                    <span>270</span>/<span>300</span>
                  </div>
                </div>
              </div>
            </Col>
            <Col sm="6" className="p-0">
              <div className="user-info">
                <div className="username-dropdown">
                  <Dropdown>
                    <Dropdown.Toggle id="dropdown-basic">
                      Adam <img src={userIcon} alt="" />
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item onClick={() => navigate("/app/profile")}>
                        {t("Profile.Title")}
                      </Dropdown.Item>
                      <Dropdown.Item onClick={() => logout()}>
                        {t("Home.Logout")}
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </Col>
            <Col sm="12" className="p-0">
              <div className="ranking">
                <img
                  src={RankBadge}
                  onClick={() => navigate("/app/leaderboard")}
                  alt=""
                />
                <div className="rank-score">
                  <span>{t("Home.Rank")}</span> <strong>24</strong>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
        <Container className="p-0" fluid>
          <Row className="footer">
            <Col sm="12">
              <div className="text-center play-button">
                <Button
                  className="play"
                  onClick={() => navigate("/app/categories/0")}
                >
                  {t("Home.Play")}
                </Button>
              </div>
            </Col>
          </Row>
          <Row className="footer">
            <Col className="p-0" lg="12">
              <Footer />
            </Col>
          </Row>
        </Container>
      </div>
      <BadgesModal show={modalShow} onHide={() => setModalShow(false)} />
    </>
  );
};

export default Home;
