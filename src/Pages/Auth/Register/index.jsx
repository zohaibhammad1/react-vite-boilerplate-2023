import { useState, useEffect, useRef } from "react";
import "./style.scss";
import { InputGroup, Button } from "react-bootstrap";
import Image from "Assets/Images/Auth/girl-img1.svg";
import Image2 from "Assets/Images/Auth/girl-img3.svg";
import { Link } from "react-router-dom";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { registerSchema } from "Utils/ValidationSchemas";
import { useDispatch, useSelector } from "react-redux";
import { getRoles, registerUser } from "Redux/Auth/RegisterReducer";
import { setLoader } from "Redux/CommonReducer";
import { useTranslation } from "react-i18next";
import { VerificationModal } from "Components";
import { EMAIL_VERIFICATION } from "Constants";
import { appendEmailDomain } from "Utils";

const Register = () => {
  const [modalShow, setModalShow] = useState(false);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const formikRef = useRef();
  const { isLoading, roles } = useSelector((state) => state.RegisterReducer);

  useEffect(() => {
    dispatch(getRoles());
  }, []);
  useEffect(() => {
    dispatch(setLoader(isLoading));
    if (!isLoading) {
      formikRef.current.values.role = roles[0]?.id;
    }
  }, [isLoading, roles]);

  const initialValues = {
    role: "",
    email: "",
    user_name: "",
    password: "",
    password_confirmation: "",
  };

  const handleSubmit = async (
    values,
    { setSubmitting, setErrors, resetForm }
  ) => {
    const res = await dispatch(registerUser(appendEmailDomain(values)));
    setErrors(res.payload.errors);
    setSubmitting(false);
    if (!res.payload.error) {
      setModalShow(true);
      resetForm();
    }
  };

  return (
    <>
      <div className="auth-bg-img register-page">
        <div className="auth-page">
          <div className="d-flex justify-content-center align-items-end mvh-100">
            <div className="left-girl">
              <img src={Image} alt="img-1" />
            </div>
            <div className="form">
              <div className="auth-form-box bg-signup rounded">
                <div className="box-header text-center">
                  <h2>{t("Register.Title")}</h2>
                </div>
                <div className="authbox-content">
                  <Formik
                    initialValues={initialValues}
                    validationSchema={registerSchema}
                    onSubmit={handleSubmit}
                    innerRef={formikRef}
                  >
                    <Form>
                      <label htmlFor="role">
                        {t("Register.Form.UserRole")}
                      </label>
                      <Field as={"select"} name={"role"}>
                        {roles.map((value, index) => (
                          <option key={index} value={value.id}>
                            {value.title}
                          </option>
                        ))}
                      </Field>
                      <ErrorMessage
                        name={"role"}
                        component={"div"}
                        className={"validation-message"}
                      />

                      <label htmlFor="email">{t("Register.Form.Email")}</label>
                      <InputGroup>
                        <Field name={"email"} type={"text"} />
                        <InputGroup.Text id="basic-addon2">
                          @dijklander.nl
                        </InputGroup.Text>
                      </InputGroup>
                      <ErrorMessage
                        name={"email"}
                        component={"div"}
                        className={"validation-message"}
                      />

                      <label htmlFor="user_name">
                        {t("Register.Form.Username")}
                      </label>
                      <Field type="text" name={"user_name"} />
                      <ErrorMessage
                        name={"user_name"}
                        component={"div"}
                        className={"validation-message"}
                      />

                      <label htmlFor="password">
                        {t("Register.Form.Password")}
                      </label>
                      <Field type="password" name={"password"} />
                      <ErrorMessage
                        name={"password"}
                        component={"div"}
                        className={"validation-message"}
                      />

                      <label htmlFor="password_confirmation">
                        {t("Register.Form.ConfirmPassword")}
                      </label>
                      <Field type="password" name={"password_confirmation"} />
                      <ErrorMessage
                        name={"password_confirmation"}
                        component={"div"}
                        className={"validation-message"}
                      />

                      <Button variant="primary" type="submit">
                        {t("Buttons.Create")}
                      </Button>
                    </Form>
                  </Formik>
                </div>
              </div>
              <div className="bottom-link text-center">
                <Link to="/login">{t("Register.LoginLink")}</Link>
              </div>
            </div>
            <div className="right-girl">
              <img src={Image2} alt="img-3" />
            </div>
          </div>
        </div>
      </div>
      <VerificationModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        email={formikRef.current?.values?.email}
        type={EMAIL_VERIFICATION}
      />
    </>
  );
};

export default Register;
