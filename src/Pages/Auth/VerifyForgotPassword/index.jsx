import React from "react";
import "./style.scss";
import { Form } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import Image from "Assets/Images/Auth/girl-img1.svg";
import Image2 from "Assets/Images/Auth/girl-img3.svg";
import { Link, useNavigate } from "react-router-dom";

const VerifyForgotPassword = () => {
  const navigate = useNavigate()


  return (
    <div>
      <div className="auth-bg-img verifyemail-page">
        <div className="auth-page">
          <div className="d-flex justify-content-center align-items-end mvh-100">
            <div className="left-girl">
              <img src={Image} alt="img-1" />
            </div>
            <div className="authform">
              <div className="auth-form-box bg-signup rounded">
                <div className="box-header mt-2 text-center">
                  <h2>FORGOT PASSWORD</h2>
                  <p className="text-white fs-20">
                    Code is sent to xyz@dijklander.nl
                  </p>
                </div>
                <div className="authbox-content pt-2 mt-5">
                  <form action="">
                    <Form.Group
                      as={Row}
                      className="mt-5 pt-5 verify-email"
                      controlId="verifyEmail"
                    >
                      <Col md="3">
                        <Form.Control type="text" />
                      </Col>
                      <Col md="3">
                        <Form.Control type="text" />
                      </Col>
                      <Col md="3">
                        <Form.Control type="text" />
                      </Col>
                      <Col md="3">
                        <Form.Control type="text" />
                      </Col>
                    </Form.Group>

                    <div className="mb-5 text-center mt-3">
                      <Link
                        className="text-white text-decoration-underline"
                        to="/login"
                      >
                        Didn't receive code? Request again
                      </Link>
                    </div>

                    <div className="pt-3">
                      <Button variant="primary" type="submit" onClick={() => navigate('/reset-password')}>
                        Update
                      </Button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div className="right-girl">
              <img src={Image2} alt="img-3" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VerifyForgotPassword;
