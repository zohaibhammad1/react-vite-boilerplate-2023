import { Modal, Button } from "react-bootstrap";
import "./style.scss";
import { useTranslation } from "react-i18next";
import { EMAIL_VERIFICATION } from "Constants/index";
import { useNavigate } from "react-router-dom";

const VerificationModal = ({ onHide, show, email, type }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  const handleClick = (e) => {
    e.preventDefault();
    navigate("/");
    onHide();
  };

  return (
    <>
      <div className="badges-modal-wrapper">
        <Modal
          onHide={onHide}
          show={show}
          size="lg"
          className="verification-modal"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header className="text-center" closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              {type === EMAIL_VERIFICATION
                ? t("VerificationModal.Title")
                : t("ForgotPassword.Title")}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="divider-dual"></div>
            <div className="modal-desc">
              {type === EMAIL_VERIFICATION
                ? t("VerificationModal.EmailHeader")
                : t("VerificationModal.PasswordHeader")}{" "}
              {email}
            </div>
            <Button className="modal-btn" onClick={handleClick}>
              {t("Login.Title")}
            </Button>
            <div className="request-link">{t("VerificationModal.Link")}</div>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
};

export default VerificationModal;
