export { default as Footer } from "./Footer";
export { default as BadgesModal } from "./BadgesModal";
export { default as TableBaseComponent } from "./TableBaseComponent";
export { default as AnswersModal } from "./AnswersModal";
export { default as RotatePhone } from "./RotatePhone";
export { default as LevelsComponent } from "./LevelsComponent";
export { default as VerificationModal } from "./VerificationModal";
export { default as PaginationComponent } from "./PaginationComponent";
