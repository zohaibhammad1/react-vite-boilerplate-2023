import { ProgressBar } from "react-bootstrap";
import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";

const LevelsComponent = () => {
  const navigate = useNavigate();
  const params = useParams();
  const { t } = useTranslation();
  const { categories } = useSelector((state) => state.CategoryReducer);

  const navigation = (item) => {
    if (item?.progress_percentage >= 100) navigate(`/app/result/${item?.id}`);
    else if (item?.progress_percentage > 0 && item?.progress_percentage < 100)
      navigate(
        `/app/questions/${item?.unattempted_que_id}?levelId=${item?.id}`
      );
    else navigate(`/app/introduction/${item?.id}`);
  };

  return (
    <div className="level-boxes">
      {categories[params.id]?.levels.map((item, index) => (
        <div key={index} className="level" onClick={() => navigation(item)}>
          <div className="level-text">{t("Categories.Level")}</div>
          <h1>{index + 1}</h1>
          <ProgressBar now={item?.progress_percentage} />
        </div>
      ))}
    </div>
  );
};

export default LevelsComponent;
