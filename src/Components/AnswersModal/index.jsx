import { Modal, Accordion } from "react-bootstrap";
import RadioImg from "Assets/Images/icons/radio.svg";
import WorngAns from "Assets/Images/new-assets/red.png";
import CorrectAns from "Assets/Images/new-assets/green.png";
import "./style.scss";
import { useTranslation } from "react-i18next";

const BadgesModal = (props) => {
  const { t } = useTranslation();

  return (
    <>
      <div>
        <Modal
          {...props}
          size="lg"
          className="answers-modal"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header className="text-center" closeButton>
            <Modal.Title>
              <h2>
                {t("Categories.Level")} 1 {t("Answers.Title")}
              </h2>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Accordion>
              <Accordion.Item eventKey="0">
                <Accordion.Header>
                  <span>1</span>
                  <div className="question">
                    Aliquam in bibendum mauris. Sed vitae erat vel velit blandit
                    pharetra vitae nec ante. Cras at est augue. Cras ut ...
                    interdum elit. Ut malesuada a urna sit amet blandit?
                  </div>
                </Accordion.Header>
                <Accordion.Body>
                  <div className="answers-wrapper">
                    <div className="answers">
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={WorngAns} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                    </div>
                    <div className="points">{t("Answers.Points")}: 20</div>
                  </div>
                  <div className="correct-answer">
                    ({t("Answers.CorrectAnswer")} Answer 3)
                  </div>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="1">
                <Accordion.Header>
                  <span>2</span>Accordion Item #2
                </Accordion.Header>
                <Accordion.Body>
                  <div className="question">
                    Aliquam in bibendum mauris. Sed vitae erat vel velit blandit
                    pharetra vitae nec ante. Cras at est augue. Cras ut ...
                    interdum elit. Ut malesuada a urna sit amet blandit?
                  </div>
                  <div className="answers-wrapper">
                    <div className="answers">
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={CorrectAns} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                    </div>
                    <div className="points">Points: 20</div>
                  </div>
                  <div className="correct-answer">
                    (Correct answer is Answer 3)
                  </div>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="2">
                <Accordion.Header>
                  <span>3</span>Accordion Item #3
                </Accordion.Header>
                <Accordion.Body>
                  <div className="question">
                    Aliquam in bibendum mauris. Sed vitae erat vel velit blandit
                    pharetra vitae nec ante. Cras at est augue. Cras ut ...
                    interdum elit. Ut malesuada a urna sit amet blandit?
                  </div>
                  <div className="answers-wrapper">
                    <div className="answers">
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={WorngAns} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                    </div>
                    <div className="points">Points: 20</div>
                  </div>
                  <div className="correct-answer">
                    (Correct answer is Answer 3)
                  </div>
                </Accordion.Body>
              </Accordion.Item>
              <Accordion.Item eventKey="3">
                <Accordion.Header>
                  <span>4</span>Accordion Item #4
                </Accordion.Header>
                <Accordion.Body>
                  <div className="question">
                    Aliquam in bibendum mauris. Sed vitae erat vel velit blandit
                    pharetra vitae nec ante. Cras at est augue. Cras ut ...
                    interdum elit. Ut malesuada a urna sit amet blandit?
                  </div>
                  <div className="answers-wrapper">
                    <div className="answers">
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={WorngAns} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                      <div className="options">
                        <img src={RadioImg} alt="" />
                        <span>Answer 1</span>
                      </div>
                    </div>
                    <div className="points">Points: 20</div>
                  </div>
                  <div className="correct-answer">
                    (Correct answer is Answer 3)
                  </div>
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
};

export default BadgesModal;
