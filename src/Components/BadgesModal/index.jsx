import { Modal, ProgressBar } from "react-bootstrap";
import "./style.scss";
import MainBadge from "Assets/Images/new-assets/silver-L.png";
import SilverBadge from "Assets/Images/new-assets/silver-s.png";
import BronzeBadge from "Assets/Images/new-assets/bronze-s.png";
import GoldBadge from "Assets/Images/new-assets/gold-s.png";
import DaimondBadge from "Assets/Images/new-assets/diamond-s.png";
import PlatiniumBadge from "Assets/Images/new-assets/platinum-s.png";
import { useTranslation } from "react-i18next";

const BadgesModal = (props) => {
  const { t } = useTranslation();

  return (
    <>
      <div className="badges-modal-wrapper">
        <Modal
          {...props}
          size="lg"
          className="badges-modal"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header className="text-center" closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              {t("Badges.Title")}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="main-badge">
              <div className="badge-name">Silver</div>
              <img src={MainBadge} alt="" />
              <div className="current-score">
                <ProgressBar now={60} />
                <div className="score">
                  <span>270</span>/<span>300</span>
                </div>
              </div>
            </div>
            <div className="divider-dual"></div>
            <div className="badges-grid">
              <div className="badge-item active">
                <img src={BronzeBadge} alt="" />
                <div className="semibold">Bronze</div>
                <div className="light">0-100</div>
              </div>
              <div className="badge-item">
                <img src={SilverBadge} alt="" />
                <div className="semibold">Silver</div>
                <div className="light">100-300</div>
              </div>
              <div className="badge-item">
                <img src={GoldBadge} alt="" />
                <div className="semibold">Gold</div>
                <div className="light">300-600</div>
              </div>
              <div className="badge-item">
                <img src={DaimondBadge} alt="" />
                <div className="semibold">Diamond</div>
                <div className="light">600-1000</div>
              </div>
              <div className="badge-item active">
                <img src={PlatiniumBadge} alt="" />
                <div className="semibold">Platinum</div>
                <div className="light">1000-1500</div>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
};

export default BadgesModal;
