import {
  Login,
  Register,
  // ForgotPassword,
  // ResetPassword,
  // VerifyEmail,
  // VerifyForgotPassword,
} from "Pages";

//Index routes can't contain child routes.

//For using child routes, use Outlet in the parent component.

const PublicRoutes = [
  {
    component: <Login />,
    index: true,
  },
  {
    component: <Register />,
    path: "register",
  },
  // {
  //   component: <Login />,
  //   path: "login",
  // },
  // {
  //   component: <ForgotPassword />,
  //   path: "forgot-password",
  // },
  // {
  //   component: <ResetPassword />,
  //   path: "reset-password",
  // },
  // {
  //   component: <VerifyEmail />,
  //   path: "verify-email",
  // },
  // {
  //   component: <VerifyForgotPassword />,
  //   path: "verify-forgot",
  // }
  // {
  //   component: <Test />,
  //   path: "test",
  //   children: [
  //     {
  //       component: <Home />,
  //       path: "home",
  //       children: [{ component: <Login />, path: "about" }],
  //     },
  //     {
  //       component: <Login />,
  //       path: "about",
  //     },
  //   ],
  // },
];

export default PublicRoutes;
