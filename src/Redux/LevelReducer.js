import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import apiCall from "../Middlewares/ApiClient";
import { apiCallTypes } from "Constants/index";

const initialState = {
  isLoading: false,
  level: {},
};

export const LevelAction = createAsyncThunk(
  "Level/LevelAction",
  async (payload, thunkApi) => {
    const res = await apiCall(`/my/level/${payload}`, apiCallTypes.get);

    if (res.error !== true) {
      return res;
    }
    return thunkApi.rejectWithValue(res);
  }
);

export const LevelSlice = createSlice({
  name: "LevelReducer",
  initialState,
  extraReducers: {
    [LevelAction.fulfilled]: (state, { payload }) => {
      state.level = payload?.data?.data;
      state.isLoading = false;
    },
    [LevelAction.rejected]: (state) => {
      state.isLoading = false;
    },
    [LevelAction.pending]: (state) => {
      state.isLoading = true;
    },
  },
});

export default LevelSlice.reducer;
